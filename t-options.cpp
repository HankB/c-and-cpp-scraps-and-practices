/*
driver for exercising/testing options.[cpp|h] 

Build
gcc -Wall -o t-options -I. t-options.cpp options.cpp 

*/

#include <options.h>

int main( int argc, char** argv)
{
    cmd_line_options    opts;

    return opts.parse(argc, argv);
}