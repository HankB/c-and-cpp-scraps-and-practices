# C and C++ scraps and practices

Miscellaneous C/C++ samples and some practices.

## Motivation

I have `.../C` and `.../C++` directories where I put junk I'm trying out. Might as well put them somewhere where it is easy to copy them to various PCs. Also I plan to record some policies and "best practices" so I can recall what I used for testing a year or two down the road and so on. And links to personal projects where I used these so I can see what I did long after I've forgotten the details.

## Policies

* Compile with the `-Wall` switch (or equivalent for the tol chain.)
* Unit test as appropriate.
  * Catch2 for C++ (see https://github.com/HankB/game_of_life/C++)
  * CUnit for C (see https://github.com/HankB/game_of_life/C)
* Style - always a subject worthy of discussion. I should pick out a style that I cna format automatically with VS Code.
* Commentary - Describe breifly what each file is for as I don;t plan to document that here.
* Building ... Include a comment with the command like tb build, e.g. `g++ -Wall -o hello hello.cpp`

## Contributing

Sure. Why not. Go ahead and submit a PR and I'll consider and probably merge it.

## Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

## License

MIT, no beer clause.

## Project status

Nothing here yet, and this will never be finished as long as I live and can type.
