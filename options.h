
/* encapsulate command line options
*/
class cmd_line_options
{
    unsigned int        buff_size;
    unsigned int        sample_count;
    unsigned int        write_count;
public:
    cmd_line_options(): buff_size(4096), sample_count(512), write_count(1024) {}
    int parse(int argc, char **argv);
};