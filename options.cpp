/*
Starter file for processing (and testing processing) command line args for 
https://gitlab.com/HankB/disk_profile

options.h and options.cpp will be exercised by t-options.cpp which
has the build command.

*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include "options.h"

// parsing havily cribbed from getopts man page
int cmd_line_options::parse(int argc, char** argv)
{
    int c;

    while (1)
    {
        int option_index = 0;
        static struct option long_options[] = {
            {"buff_size", required_argument, 0, 'b'},
            {"sample_count", required_argument, 0, 's'},
            {"write_count", required_argument, 0, 'w'},
            {0, 0, 0, 0}
            };

        c = getopt_long(argc, argv, "b:s:w",
                        long_options, &option_index);
        printf("c = %d, %c\n", c, c);
        if (c == -1)
            break;

        char*   endbuf = 0;
        int     decoded_val;

        switch (c)
        {
        case 0:
            printf("case 0, option %s", long_options[option_index].name);
            if (optarg)
                printf(" with arg %s", optarg);
            printf("\n");
            break;

        case 'b':
            decoded_val = strtoul(optarg, &endbuf, 10);
            printf("option b with value '%s' (%d, %ld)\n", optarg, decoded_val, endbuf-optarg);
            if( (endbuf-optarg == 0) || 
                (strlen(optarg) > (size_t)(endbuf-optarg)) ||
                (decoded_val <= 0))
            {
                fprintf( stderr, "cannot decode \"%s\" as a valid buffer size\n", optarg);
                return EXIT_FAILURE;
            }
            else
            {
                buff_size = decoded_val;
            }
            break;

        case 's':
            printf("option s with value '%s'\n", optarg);
            break;

        case 'w':
            printf("option w with value '%s'\n", optarg);
            break;

        case '?':
            printf( "case '?' error processing arg\n");
            return EXIT_FAILURE;

        default:
            printf("?? getopt returned character code 0%o ??\n", c);
        }
    }

    if (optind < argc)
    {
        printf("non-option ARGV-elements: ");
        while (optind < argc)
            printf("%s ", argv[optind++]);
        printf("\n");
    }

    return EXIT_SUCCESS;
}